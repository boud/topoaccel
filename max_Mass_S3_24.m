/*
# max_Mass_S3_24: maxima script for calculating topological acceleration
# induced by spatial topology for M_6 (Cavicchioli 2009), with
# a homogeneous 24-tiling of S^3 
#
# Copyright (C) 2009, 2021, 2022 Boud Roukema
# Copyright (C) 2021, 2022 Quentin Vigneron
#
# This program is free software; you can
# redistribute it and/or modify it under the
# terms of the GNU General Public License as
# published by the Free Software Foundation;
# either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU
# General Public License along with this
# program; if not, see:
#       http://www.gnu.org/licenses/gpl.html
*/

/* Version: 1.1 */

/* Feb 2009 Many improvements thanks to
   Piotr T. Rozanski */

/* Remove duplicates by converting a list to a set, and
   then back to a list */
deleteduplicates(x) := listify(setify(x));

/* # Avoid imaginary sqrt ambiguities */
radexpand: true;
normalise_v4(x) := [x[1]/sqrt(x.x), x[2]/sqrt(x.x), x[3]/sqrt(x.x), x[4]/sqrt(x.x)];

/* # Normalised tangent at 4-vector a pointing towards 4-vec b" */
tangent_S3_at_a(a, b) := normalise_v4(b - ((a.b)*a));

/* # Spherical-newtonian acceleration at a from image at b */
pot_at_a(a,b)   := -a.b/sqrt(1 - (a.b)^2) * (1 - acos(a.b)/%pi);

/* Defines the list of list called 'perminus' that will do de permutation of the minus sign to define all the images. */
perminus : [[ 1, 1, 1, 1],
        [ 1, 1, 1,-1],
        [ 1, 1,-1, 1],
        [ 1,-1, 1, 1],
        [ 1, 1,-1,-1],
        [ 1,-1, 1,-1],
        [ 1,-1,-1, 1],
        [ 1,-1,-1,-1]];
lper : length(perminus);
for i:1 thru lper step 1 do
        (perminus : append(perminus, [-perminus[i]]));

/* ################################################### */
/* Creates the position of all the images in Allimages */
/* ################################################### */
Allimages : [];

/* Add the different family of images (see Durk et al. 2017, section 3.1, or the wikipedia page "Regular 4-polytope") */
/* From [1,0,0,0] */
for i:1 thru length(perminus) step 1 do
        (Allimages : append(Allimages, [perminus[i]*[1,0,0,0]])
);
/* From [1/2,1/2,1/2,1/2] */
for i:1 thru length(perminus) step 1 do
        (Allimages : append(Allimages, [perminus[i]*[1/2, 1/2, 1/2, 1/2]])
);
Allimages : deleteduplicates(Allimages);

ll : length(Allimages);
for i:1 thru ll step 1 do
        (for j:1 thru length(permutations(Allimages[i])) step 1 do
                (Allimages : append(Allimages, [listify(permutations(Allimages[i]))[j]])
        )
);

/* Delete duplicates and order by layers */
Allimages : reverse(sort(deleteduplicates(Allimages)));

/* #################################################### */
/* Calculation of the expansion series of the potential */
/* #################################################### */

print("");
print("-------------------------------------------");
print("Calculation of the potential for", length(Allimages), "images");
print("-------------------------------------------");
print("");

vec_m : [ cos(rr), sin(rr)*x, sin(rr)*y , sin(rr)*sqrt(1-x^2-y^2)];
/* vec_m : [ cos(rr), sin(rr)*cos(theta), sin(rr)*sin(theta)*cos(phi) , sin(rr)*sin(theta)*sin(phi)]; */

nn : 5;
potF : 0;
for i:1 thru length(Allimages) step 1 do
        (potF : potF + taylor(pot_at_a(vec_m,Allimages[i]),rr,0,nn)
);

Rscalar : 6;
N_tiles : 24;
FD_vol : 2*%pi^2/N_tiles;

/* Alternative version of the full potential, with the -1th order
   removed, and with 1/2\pi added for each image.
*/
potFullAlt: 1/rr + potF + N_tiles/(2*%pi);

print(potF);
print("");
print("Phi_0*V^1/3 for the integral convention: ",bfloat(coeff(expand(potFullAlt*FD_vol^(1/3)),rr,0)));
print("");
print_normalised_terms("24-cell",potF, Rscalar, FD_vol, true);

claimed_value : (9-4*sqrt(3)*%pi)/(108*%pi);
print_normalised_constant("24-cell",potF, Rscalar, N_tiles, claimed_value);
