/*
# max_Mass_S3_1: maxima script for evaluating
# topological acceleration in S^3 (one image only).
#
# Copyright (C) 2009, 2021, 2022 Boud Roukema
# Copyright (C) 2021, 2022 Quentin Vigneron
#
# This program is free software; you can
# redistribute it and/or modify it under the
# terms of the GNU General Public License as
# published by the Free Software Foundation;
# either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU
# General Public License along with this
# program; if not, see:
#       http://www.gnu.org/licenses/gpl.html
*/

/* Version: check for the git commit hash */

/* Feb 2009 Many improvements thanks to
   Piotr T. Rozanski */

pot_at_a(a,b)   := -a.b/sqrt(1 - (a.b)^2) * (1 - acos(a.b)/%pi);

/* ################################################### */
/* Creates the position of all the images in Allimages */
/* ################################################### */
Allimages : [[1, 0, 0, 0]];

ll : length(Allimages);


/* #################################################### */
/* Calculation of the expansion series of the potential */
/* #################################################### */

print("");
print("-------------------------------------------");
print("Calculation of the potential for", length(Allimages), "image");
print("-------------------------------------------");
print("");

vec_m : [ cos(rr), sin(rr)*x, sin(rr)*y , sin(rr)*sqrt(1-x^2-y^2)];
/* vec_m : [ cos(rr), sin(rr)*cos(theta), sin(rr)*sin(theta)*cos(phi) , sin(rr)*sin(theta)*sin(phi)]; */

nn : 5;
potF : 0;
for i:1 thru length(Allimages) step 1 do
        (potF : potF + taylor(pot_at_a(vec_m,Allimages[i]),rr,0,nn)
);


Rscalar : 6;
N_tiles : 1;
FD_vol : 2*%pi^2/N_tiles;

/* Alternative version of the full potential, with the -1th order
   removed, and with 1/2\pi added for each image.
*/
potFullAlt: 1/rr + potF + N_tiles/(2*%pi);

print(potF);
print("");
print("Phi_0*V^1/3 for the integral convention: ",bfloat(coeff(expand(potFullAlt*FD_vol^(1/3)),rr,0)));
print("");

print_normalised_terms("1-cell",potF, Rscalar, FD_vol, true);

claimed_value : 1/%pi; /* claimed value of integration constant */
print_normalised_constant("1-cell",potF, Rscalar, N_tiles, claimed_value);
