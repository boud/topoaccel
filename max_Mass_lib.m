/*
# max_Mass_lib: maxima script library functions for evaluating the
# residual gravity acceleration effect.
#
# Copyright (C) 2009, 2021, 2022 Boud Roukema
# Copyright (C) 2021, 2022 Quentin Vigneron
#
# This program is free software; you can
# redistribute it and/or modify it under the
# terms of the GNU General Public License as
# published by the Free Software Foundation;
# either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU
# General Public License along with this
# program; if not, see:
#       http://www.gnu.org/licenses/gpl.html
*/

/* Version: check for the git commit hash */


/* Sanity check for anisotropy: evaluate the coefficient for a few
   different arbitrary (x,y) pairs of tangential directions; if any of
   the substitutions are significantly different, then return 'false',
   otherwise return 'true'.

   Since the default is 'isotropic', this may, in principle, fail to
   detect anisotropy.  */

is_anisotropic(tayl_coeff) := (
  [tol, c1, c2, c3, c4],
  tol: 1e-10,
  c1: subst([x=0.12, y=0.45], tayl_coeff),
  c2: subst([x=0.347, y=-0.79], tayl_coeff),
  c3: subst([x=0.6, y=0.2], tayl_coeff),
  c4: subst([x=-0.012, y=0.45], tayl_coeff),
  if abs(c1 - c2) > tol or abs(c2 - c3) > tol
    or abs(c3 - c4) > tol then anisotropic : true
    else anisotropic: false,
  anisotropic);


/* Print out the -1th, 1st, 2nd, 3rd, 4th, 5th terms of the Maclaurin
   expansion of a topological acceleration */
/* Comment: 6*Rscal is in practice 1, so has no practical effect. */
print_normalised_terms(name,pot_tayl,Rscal,vol,dofifth) := (
  orderminus1 : coeff(pot_tayl,rr,-1),
  order1 : coeff(pot_tayl,rr,1) / (Rscal/6),
  order2 : coeff(pot_tayl,rr,2) * vol,
  order3 : coeff(pot_tayl,rr,3) /(Rscal/6)^2,
  order4 : coeff(pot_tayl,rr,4) /(Rscal/6) *vol,
  order5 : coeff(pot_tayl,rr,5) /(Rscal/6)^3,
  printf(true, "~%~%"),
  printf(true, "~%Tab2 ~a \& ",name),
  printf(true, "~a/r \& ", orderminus1),
  if is_anisotropic(order1) then printf(true, " -- \& ") else printf(true, "~a r \& ", order1),
  if is_anisotropic(order2) then printf(true, " -- \& ") else printf(true, "~a r^2 \& ", order2),
  if is_anisotropic(order3) then printf(true, " -- \& ") else printf(true, "~a r^3 \& ", order3),
  if dofifth and not is_anisotropic(order4) then printf(true, "~a r^4 \& ", order4) else printf(true, " -- \& "),
  if dofifth and not is_anisotropic(order5) then printf(true, "~a r^5  ", order5) else printf(true, " -- "),
  printf(true, "~%Tab2 alternative for 4th order: ~a r^4 \n", bfloat(expand(order4*45/(2*%pi)))),
  printf(true, "~%~%") );

print_normalised_constant(name,pot_tayl,Rscal,N_tiles,claimed_value) := (
  order0 : coeff(pot_tayl,rr,0) /N_tiles,
  order0_num : float(floor(100000*float(order0))/100000),
  tol : 1e-10,
  if abs(bfloat(order0 - claimed_value)) < 1e-10 then agrees: true else agrees: false,
  printf(true, "~%~%"),
  printf(true, "~%Tab3 ~a \& ",name),
  printf(true, "~a \& ", order0),
  printf(true, "~a ", order0_num),
  printf(true, " %%VALID?: ~a ", agrees),
  printf(true, "~%~%") );
