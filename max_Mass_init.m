/*
# max_Mass_init: maxima script initialisation for evaluating the
# residual gravity acceleration effect.
#
# Copyright (C) 2022 Boud Roukema, Quentin Vigneron
#
# This program is free software; you can
# redistribute it and/or modify it under the
# terms of the GNU General Public License as
# published by the Free Software Foundation;
# either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU
# General Public License along with this
# program; if not, see:
#       http://www.gnu.org/licenses/gpl.html
*/

/* Version: check for the git commit hash */

/* This file initialises conditions and properties of variables
   that should be in common for all the main scripts. 

   As of Maxima 5.44.0-3/GNU Common Lips 2.6.12 (2022), 
   the 'taylor' function appears to *not* need these, but
   future improved versions are likely to need them, 
   since they are mathematically needed for the Taylor
   (Maclaurin) expansion to be defined.

   As of Maxima 5.44.0-3/GNU Common Lips 2.6.12 (2022),
   'taylor' appears to effectively assume that the user
   wants an expansion for a one-sided, positive real
   valued range of 'rr', even though a two-sided or
   complex plane range of 'rr' gives derivatives that
   are discontinuous at 0. See
   https://sourceforge.net/p/maxima/bugs/3922 .
*/

/* Use the principal branch for square roots. See e.g.
   https://en.wikipedia.org/wiki/Principal_value#Square_root
*/
numer_pbranch: true;

/* Assume that rr, and more specifically for simplifications,
   sin(rr), is positive and real. */
declare(rr, real);
assume(rr > 0, sin(rr) > 0);

/* Constraints on x, y. */
declare(x, real);
declare(y, real);

/* Do not print out all the events of a floating point number
   being replaced by a rational. */
ratprint: false;