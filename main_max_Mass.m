/*
# main_max_Mass: main maxima script for evaluating
# topological acceleration in S^3 quotient spaces.
#
# Copyright (C) 2009, 2021, 2022 Boud Roukema
# Copyright (C) 2021, 2022 Quentin Vigneron
#
# This program is free software; you can
# redistribute it and/or modify it under the
# terms of the GNU General Public License as
# published by the Free Software Foundation;
# either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU
# General Public License along with this
# program; if not, see:
#       http://www.gnu.org/licenses/gpl.html
*/

/* Version: check for the git commit hash */

/* Feb 2009 Many improvements thanks to
Piotr T. Rozanski */

/* Ways of running this set of files include:

maxima -b main_max_Mass.m

maxima < main_max_Mass.m

maxima < main_max_Mass.m > logfile

*/

load(atensor)$
load(atrig1)$ /* more trig rules */
load(ntrig)$  /* rules for sin( n %pi/10 ) and so on */
batchload("max_Mass_init.m");
batchload("max_Mass_lib.m");
batchload("max_Mass_S3_1.m");
/* batchload("max_Mass_S3_5.m"); */
batchload("max_Mass_S3_8.m");
/* batchload("max_Mass_S3_16.m"); */
batchload("max_Mass_S3_24.m");
batchload("max_Mass_S3_120.m");
/* batchload("max_Mass_S3_600.m"); */
