N_Sigma : [1, 8, 24, 120];
Om_k : 0.05;
Rscalar : 6*Om_k/2.99792458^2;    /* h^2 Gpc^{-2}; */
R_c : sqrt(6/Rscalar);   /* curvature radius of S^3 */
phim1 : -1;              /* phi_{-1} */
phi1 : Rscalar/18;       /* phi_1 */
vol : (2*%pi^2 * R_c^3) /N_Sigma;
phi2 : -2*%pi/3 / vol;
phi3 : (Rscalar/6)^2 /45;
phi4 : -2*%pi/45 *Rscalar/6 / vol;
r_h : 0.1;

ratio_1 : phi1/phim1 * r_h^2, bfloat;
ratio_2 : phi2/phim1 * r_h^3, bfloat;
ratio_3 : phi3/phim1 * r_h^4, bfloat;
ratio_4 : phi4/phim1 * r_h^5, bfloat;

printf(true, "~%~%ratio_1: ~9,3e ~%~%", ratio_1);
printf(true, "~%~%ratio_2: ~9,3e ~9,3e ~9,3e ~9,3e ~%~%",
  ratio_2[1],ratio_2[2],ratio_2[3],ratio_2[4]);
printf(true, "~%~%ratio_3: ~9,3e ~%~%", ratio_3);
printf(true, "~%~%ratio_4: ~10,3e ~10,3e ~10,3e ~10,3e ~%~%",
  ratio_4[1],ratio_4[2],ratio_4[3],ratio_4[4]);
