topoaccel: maxima scripts for evaluating topological acceleration
=================================================================
Copyright (C) 2009, 2021, 2022 Boud Roukema, GPL-2+
Copyright (C) 2021, 2022 Quentin Vigneron, GPL-2+

Scroll to the bottom for a GPL-2+ guideline.

As of December 2021, these scripts calculate topological acceleration
for several of the S^3 quotient spaces that we call 'regular', in that
they have a Platonic solid as one of their fundamental domain shapes,
and are globally homogeneous. See Vigneron & Roukema,
[ArXiv:2201.09102](https://arXiv.org/abs/2201.09102) for details.


Testing
=======
* 20 Jan 2022 - scripts run correctly in ten seconds or so with
Maxima-5.44.0-3 and GNU Common Lisp 2.6.12.


How to run the files
====================

Example command lines to run these Maxima scripts in a Unix-like shell:
````
maxima -b main_max_Mass.m

maxima < main_max_Mass.m

maxima < main_max_Mass.m > logfile
````

Extract the results for Tables II, III and IV:
````
for tablenum in Tab2 Tab3 Phi; do grep "^${tablenum}" logfile.20221107.1 ; printf "\n"; done
````
Cross-checking some of these lines against the published values will
still require some elementary arithmetic and the observation that a
relative numerical error of about 10^{-12.5} is reasonable.

For (27) and (28) do:
````
maxima < phi_numerical.m
````


Computer algebra resources
==========================
* Maxima
  * https://tracker.debian.org/pkg/maxima
  * https://maxima.sourceforge.io/documentation.html
* SageMath
  * https://en.wikipedia.org/wiki/SageMath
* learn about shell redirection - the < and > symbols
  * https://en.wikipedia.org/wiki/Redirection_%28computing%29



TODO: 
====

* Maxima needs help to learn how to handle sqrt{5} expressions from
cos() and sin() of angles appropriate for the dodecahedral
symmetries. The maxima library 'ntrig' would appear to be what is
needed, but the scripts currently are not able to use this to 
simplify formulae enough. The current hack is to force numerical
evaluation of square roots before the main Taylor series expansions,
and also after the calculations. Please create a git branch and
propose a merge if you find a solution.

* fixes for older/newer versions of maxima

* general tidying


Copyright
=========

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see:
http://www.gnu.org/licenses/gpl.html
